
![Web-developer](GithubBanner.png)
    
## Welcome to my world <img src="https://github.com/TheDudeThatCode/TheDudeThatCode/blob/master/Assets/Earth.gif" width="24px">

<br>

```js
class Mittelblut9 {
    constructor() {
        this.name = "Ben"
        this.age = 19
        this.discord = "Mittelblut9#1974"
        this.website = "https://www.blackdayz.de"
    }
    
    description() {
        return "I am a Junior Web Developer from Germany"
    }
    
    knowledge() {
        return {
            expert: [Javascript, Node.Js, Git, HTML5, CSS]
            good: [PHP, VUE3, JQuery, Sequelize, MySQL, Figma]
            kinda: [Shopware, Shopware5, Shopware6, Docker, Symfony]
            i_know_it: [DaVinci_Resolve]
        }
    }
    
    
    title() {
        return "🧑‍ Official trained technical design assistant"
    }
    
    isDoing() {
        return {
            title: "🧑‍ Training as an IT specialist for application development"
            where: "Webmatch GmbH"
        }
    }
    
    searching() {
        return "🔎 open source colloborations"
    }
}
```
<br>

<a href="https://twitter.com/mittelblut">
  <img align="left" alt="BlackDayz | Twitter" src="https://img.icons8.com/color/48/null/twitter--v1.png"/>
</a>
<a href="https://www.linkedin.com/in/benedikt-sauer-aab82621a/">
  <img align="left" alt="BlackDayz | Linkedin" src="https://img.icons8.com/color/48/null/linkedin-2--v1.png" />
</a>
<a href="https://www.instagram.com/blackdayz_de">
  <img align="left" alt="BlackDayz | Instagram" src="https://img.icons8.com/fluency/48/null/instagram-new.png" />
</a>
<a href="https://www.youtube.com/channel/UCVXebEQVI5N6-CV7Pnj7J8w">
  <img align="left" alt="BlackDayz | Youtube" src="https://img.icons8.com/color/48/null/youtube-play.png" />
</a>


  <img align="right" width="60%" alt="GIF" src="https://cdn-images-1.medium.com/max/1600/1*JVviONQLmDrdpISk9EC0Mg.gif" />
  
<br/><br/>

[![Top Langs](https://github-readme-stats.vercel.app/api/top-langs/?username=Mittelblut9)](https://github.com/Mittelblut9)

<p align='center'>

### Badges
<div style="display:flex; align-items:center">
    <a href="https://dev.to/mittelblut9" title="Hacktoberfest 2022"><img src="https://res.cloudinary.com/practicaldev/image/fetch/s--rX-dH2o3--/c_limit,f_auto,fl_progressive,q_80,w_180/https://dev-to-uploads.s3.amazonaws.com/uploads/badge/badge_image/206/ht-badge.png" width="80px"/> </a>
</div>
<br />

### :pushpin: Holopin Badges

[![@mittelblut9's Holopin board](https://holopin.me/mittelblut9)](https://holopin.io/@mittelblut9)

<br />

<img src="https://github-readme-streak-stats.herokuapp.com/?user=Mittelblut9&theme=tokyonight&hide_border=true" alt="mystreak"/>

[![Mittelblut's GitHub Activity Graph](https://activity-graph.herokuapp.com/graph?username=Mittelblut9&theme=react-dark&hide_border=true)](Mittelblut9)

![](https://github-profile-summary-cards.vercel.app/api/cards/profile-details?username=Mittelblut9&theme=github_dark)
</p>

<p align='center'>
  <img src="https://visitor-badge.glitch.me/badge?page_id=Mittelblut9.Mittelblut9" alt="visitor badge"/>
</p>
